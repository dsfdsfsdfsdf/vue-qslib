// 排列顺序
export enum OrderByTypes {
    obtVertical= 0,
    obtHorizontal= 1,

}

export default class  OrderBy {
    public static OrderByType: OrderByTypes;

    public TypeName: string = '纵向排列';
    public TypeValue: OrderByTypes = OrderByTypes.obtVertical;

    constructor(typeName: string, typeValue: OrderByTypes) {
        this.TypeName = typeName;
        this.TypeValue = typeValue;
    }
}
